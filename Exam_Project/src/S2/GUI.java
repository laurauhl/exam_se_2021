package S2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI extends JFrame {
    JTextField textField1 = new JTextField();
    JTextField textField2 = new JTextField();
    JTextField textField3 = new JTextField();
    JButton button = new JButton("Press");

    GUI(){
        setTitle("Count characters in text fields");
        setSize(250, 230);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
        init();
        add_widgets();
    }

    void add_widgets(){
        add(textField1); add(textField2); add(textField3); add(button);
    }

    void init(){
        setLayout(null);
        textField1.setBounds(40, 20, 150, 30);
        textField2.setBounds(40, 60, 150, 30);
        textField3.setBounds(40, 100, 150, 30);
        button.setBounds(65, 140, 100, 20);

        textField3.setEditable(false);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textField3.setText(String.valueOf(textField1.getText().length() + textField2.getText().length()));
                textField1.setText(null);
                textField2.setText(null);
            }
        });
    }

    public static void main(String[] args) {
        new GUI();
    }
}

